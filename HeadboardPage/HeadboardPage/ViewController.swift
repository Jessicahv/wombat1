//
//  ViewController.swift
//  HeadboardPage
//
//  Created by Isaac Martínez on 4/9/18.
//  Copyright © 2018 Isaac.martinez@basetis.com. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource{
    
    let features = ["#Magic Line", "MAGIC LINE", "HUHA"]
    
    let featuresBackground: [UIImage] = [
        
        UIImage(named: "bg3")!,
        UIImage(named: "bg2")!,
        UIImage(named: "bg1")!
    ]

    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        collectionView.register(UINib(nibName: "Cell1", bundle: nil), forCellWithReuseIdentifier: "Cell")

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        
        /*cell.HeadboardLabel.text = features[indexPath.item]
        cell.HeadboardImage.image = featuresBackground[indexPath.item]
        */
        return cell

}

}
