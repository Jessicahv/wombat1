//
//  CollectionViewCell.swift
//  intentoTabBar
//
//  Created by Isaac Martínez on 4/9/18.
//  Copyright © 2018 Jessica Hernandez. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var HeadboardCountdownBackgorundImage: UIImageView!
}
