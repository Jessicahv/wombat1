//
//  FirstViewController.swift
//  intentoTabBar
//
//  Created by Jessica Hernandez on 4/9/18.
//  Copyright © 2018 Jessica Hernandez. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var CollectionView: UICollectionView!
    
    let features = ["#Magic Line"]
    
    let featuresBackground: [UIImage] = [
    
        UIImage(named: "bg1")!,
        UIImage(named: "bg2")!
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return featuresBackground.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        
        cell.TitleLabel.text = features[indexPath.item]
        cell.HeadboardCountdownBackgorundImage.image = featuresBackground[indexPath.item]
        
        return cell
        
}
}

